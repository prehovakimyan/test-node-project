const conn = require('../util/config.js');
const fs = require('fs');
const path = require('path');

const p = path.join(
    path.dirname(process.mainModule.filename),
    'data',
    'cart.json',
);

module.exports = class Cart {

  static addProduct(id, cart_id, mode, product, callback ) {
    //Fetch the previous cart
    if(mode === 'add') {
      var count = 1;
      conn.query(`INSERT INTO cart_item SET cart_id='${cart_id}',product_id='${id}',count='${count}',one_price='${product.price}'`, callback);
    } else {
      conn.query(`UPDATE cart_item SET count = count + 1 WHERE cart_id='${cart_id}' AND product_id='${id}'`, callback);
    }
  }
  static findByProductId(id, cart_id, callback) {
    //Fetch the previous cart
    conn.query(`SELECT *  FROM cart_item where product_id=${id} AND cart_id=${cart_id} `, callback);

  }

  static getLastCartId(callback) {
    conn.query(`SELECT *  FROM cart where 1 ORDER BY id LIMIT 1`, callback);
  }
  static initCart(id, callback) {
    conn.query(`INSERT INTO cart SET id=${id}, creaton_date= NOW()`, callback);
  }

  static deleteProduct(id, productPrice) {
    fs.readFile(p, (err, fileContent) => {
      if (err) {
        return false;
      }
      const updatedCart = {...JSON.parse(fileContent)};
      const product = updatedCart.products.find(prod => prod.id === id);
      if(!product) {
        return false;
      }
      const productQty = product.qty;
      updatedCart.products = updatedCart.products.filter(
          prod => prod.id !== id
      );
      updatedCart.totalPrice = updatedCart.totalPrice - productPrice * productQty;
      fs.writeFile(p, JSON.stringify(updatedCart), (err) => {
        console.log(err);
      });
    });
  }

  static getProducts(cb) {
    fs.readFile(p, (err, fileContent) => {
      const cart = JSON.parse(fileContent);
      if(err) {
        cb([]);
      }
      cb(cart);
    });
  }
};