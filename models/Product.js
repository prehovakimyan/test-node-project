const conn = require('../util/config.js');
const CartModel = require('../models/Cart');

module.exports = class Product {

  constructor(id, title, imageUrl, description, price) {
    this.id = id;
    this.title = title;
    this.imageUrl = imageUrl;
    this.description = description;
    this.price = price;
  }


  static get(con, callback) {
    con.query('SELECT *  FROM products where 1', callback);
  };

  save(callback) {
    conn.query(`INSERT INTO products SET title = '${this.title}', imageUrl = '${this.imageUrl}', description = '${this.description}', price = '${this.price}'`, callback);
  }

  update(callback) {
    conn.query(`UPDATE products SET title = '${this.title}', imageUrl = '${this.imageUrl}', description = '${this.description}', price = '${this.price}' WHERE id = ${this.id}`, callback);
  }

  static delete(id, callback) {
    conn.query(`DELETE FROM products WHERE id = ${id}`, callback);
  }

  static fetchAll(callback) {
    conn.query(`SELECT *  FROM products where 1`, callback);
  }

  static findById(id, callback) {
      conn.query(`SELECT *  FROM products where id = ${id}`, callback);
  }


};
