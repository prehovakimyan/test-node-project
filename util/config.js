var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : process.env.NODE_HOST,
  port     : process.env.NODE_PORT,
  user     : process.env.NODE_USER,
  password : process.env.NODE_PASSWORD,
  database : process.env.NODE_DB
});

connection.connect(function(err) {
  if (err) throw err;
});

module.exports = connection;
