var db = require('./config.js');
var randtoken = require('rand-token');
var md5 = require('md5');
module.exports = {
  insert: async function ($table, $fields) {
    return new Promise(function (resolve, reject) {
      db.query('INSERT INTO ' + $table + ' SET ?', $fields, function (err, result) {
        if (err) throw reject();
        resolve();
      });
    })
  },
  select: async function ($table, $fields, $where) {
    return new Promise(function (resolve, reject) {
      db.query('SELECT ' + $fields + ' FROM ' + $table + ' WHERE ?', $where, function (err, result) {
        if (err) throw reject();
        resolve();
      });
    })
  },
  delete: async function ($table, $where) {
    return new Promise(function (resolve, reject) {
      db.query('DELETE FROM ' + $table + ' WHERE id = ?', $where, function (err, result) {
        if (err) throw reject();
        resolve();
      });
    })
  },
  checkAdminAuth: async function ($email, $password) {
    var $hash_password = md5($password);

    var queryString = "SELECT * FROM admin_users WHERE email = ? AND password = ?"
    var filter = [$email, $hash_password];

    return new Promise((res, rej) => {
      db.query(queryString, filter, function (err, result, fields) {
        if (err) throw err;
        res(result)
      });
    })
  },

  allOnlineUsers: async function () {

    var queryString = "SELECT * FROM users WHERE auth_token != ? ";
    var filter = '';
    return new Promise((res, rej) => {
      db.query(queryString, filter, function (err, result) {
        if (err) throw err;
        res(result)
      });
    })

  },

  allChatByUsers: async function ($user_id) {

    var queryString = "SELECT admin_users.*,chats.chat_id,chats.user_first ,chats.user_second FROM chats LEFT JOIN admin_users ON (admin_users.id = chats.user_first OR admin_users.id = chats.user_second) " +
        " WHERE (chats.user_first = ? OR chats.user_second = ?) GROUP BY chat_id";
    var filter = [$user_id, $user_id];
    return new Promise((res, rej) => {
      db.query(queryString, filter, function (err, result) {
        if (err) throw err;

        res(result)
      });
    })

  },

  getMessagesByChatId: async function ($chat_id){

    var queryString = "SELECT messages.*,admin_users.avatar as avatar FROM messages LEFT JOIN admin_users ON admin_users.id = messages.user_id WHERE messages.chat_id = ? ORDER BY id ASC ";
    return new Promise((res, rej) => {
      db.query(queryString, $chat_id, function (err, result) {
        if (err) throw err;
        res(result)
      });
    })

  },

  updateUserToken: async function ($user_id) {
    var $token = randtoken.generate(16);

    var queryString = "UPDATE users SET auth_token = ? WHERE id = ?";
    var filter = [$token, $user_id];

    return new Promise((res, rej) => {
      db.query(queryString, filter, function (err, result) {
        if (err) throw err;
        res(result)
      });
    })

  },
  updateMessage: async function (data) {
    var $message = data.message;
    var $user_from_id = data.user_from;
    var $chat_id = data.chat_id;


    var sql_insert = "INSERT INTO messages (chat_id, user_id,message) VALUES ?";

    var values = [
      [$chat_id, $user_from_id, $message]
    ];

    return new Promise((res, rej) => {
      db.query(sql_insert,[values], function (err, result) {
        if (err) throw err;
        res(result)
      });
    })
  },

  getAvatarByUserId: async function($user_id){

    var queryString = "SELECT avatar FROM admin_users WHERE id = ?";
    return new Promise((res, rej) => {
      db.query(queryString, $user_id, function (err, result) {
        if (err) throw err;
        res(result[0].avatar)
      });
    })
  },

  getAllLanguages: async function(){

    var queryString = "SELECT * FROM languages";

    return new Promise((res, rej) => {
      db.query(queryString, function (err, result) {
        if (err) throw err;
        res(result)
      });
    })
  },
  insertPage : async function( $data){

    $meta_title_am = $data.meta_title_am;
    $meta_desc_am = $data.meta_desc_am;
    $meta_key_am = $data.meta_key_am;
    if($data.meta_title_am == ''){
      $meta_title_am = $data.title_am;
    }
    if($meta_desc_am == ''){
      $meta_desc_am = $data.short_description_am;
    }



    var sql_insert = "INSERT INTO pages_am (title, short_description,description,meta_title,meta_description,meta_keyword) VALUES ?";

    var values = [
      [$data.title_am, $data.short_description_am, $data.description_am,$meta_title_am,$meta_desc_am,$meta_key_am]
    ];

    return new Promise((res, rej) => {
      db.query(sql_insert,[values], function (err, result) {
        if (err) throw err;
        res(result)
      });
    })
  },
  getAllPages : async function($table){
    var queryString = "SELECT * FROM pages_am";

    return new Promise((res, rej) => {
      db.query(queryString, $table, function (err, result) {
        if (err) throw err;
        res(result)
      });
    })
  },
  getPageInfo: async function($page_id){
    var queryString = "SELECT * FROM pages_am WHERE id= ? LIMIT 1";

    return new Promise((res, rej) => {
      db.query(queryString, $page_id, function (err, result) {
        if (err) throw err;
        res(result[0])
      });
    })
  },
  updatePage: async function(requset){
    var page_id = requset.params.page_id;

    $meta_title_am = requset.body.meta_title_am;
    $meta_desc_am = requset.body.meta_desc_am;
    $meta_key_am = requset.body.meta_key_am;
    if(requset.body.meta_title_am == ''){
      $meta_title_am = requset.body.title_am;
    }
    if($meta_desc_am == ''){
      $meta_desc_am = requset.body.short_description_am;
    }


    var queryString = "UPDATE pages_am SET title = ?, short_description = ?, description = ?, meta_title = ?, meta_description = ?, meta_keyword = ? WHERE id = ?";

    var values =[ requset.body.title_am, requset.body.short_description_am, requset.body.description_am,$meta_title_am,$meta_desc_am,$meta_key_am,page_id];

    return new Promise((res, rej) => {
      db.query(queryString, values, function (err, result) {
        if (err) throw err;
        res(result)
      });
    })

  },

  deletePage: async function($page_id){

    var queryString = "DELETE FROM pages_am WHERE id= ? ";

    return new Promise((res, rej) => {
      db.query(queryString, $page_id, function (err, result) {
        if (err) throw err;
        res(result[0])
      });
    })

  }

}