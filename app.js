require('dotenv').config();
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
var session = require('express-session');
var parseurl = require('parseurl');
const errorController = require('./controllers/Error');
var db = require('./util/database.js');
const conn = require('./util/config.js');

const app = express();


app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}));


app.use(function(req, res, next) {
  req.db = conn;

  if (!req.session.views) {
    req.session.views = {}
  }
  var sessionCartName = 'cart_id';
  req.session.views[sessionCartName] = (req.session.views[sessionCartName]) ? req.session.views[sessionCartName] : null;

  next()
});


app.set('view engine', 'ejs');
app.set('views', 'views');

const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/admin', adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

app.listen(3005);