const ProductModel = require('../models/Product');
const CartModel = require('../models/Cart');
var session = require('express-session');

exports.getProducts = async (req, res, next) => {

  $products = [];
  var getAllProducts = new Promise((res, rej) => {
    ProductModel.fetchAll(function(err, rows) {
      res(rows);
    });
  });
  $products = await getAllProducts;

  res.render('shop/product-list', {
    prods: $products,
    pageTitle: 'All Products',
    path: '/products',
    hasProducts: $products.length > 0,
    activeShop: true,
    productCss: true,
  });
};

exports.getIndex = async (req, res, next) => {
  console.log(req.session.views['cart_id']);
  $products = [];
  var getAllProducts = new Promise((res, rej) => {
    ProductModel.fetchAll(function(err, rows) {
      res(rows);
    });
  });
  $products = await getAllProducts;

  res.render('shop/index', {
    prods: $products,
    pageTitle: 'Home Page',
    path: '/',
  });

};

exports.getCart = (req, res, next) => {
  console.log("ddddd");
  console.log(req.session.views['cart_id']);
  const cartProducts = [];
  res.render('shop/cart', {
    pageTitle: 'Cart',
    path: '/cart',
    products: cartProducts,
  });
  /*CartModel.getProducts(cart => {
    const cartProducts = [];
    ProductModel.fetchAll(products => {
      for (product of products) {
        const cartProductData = cart.products.find(
            prod => prod.id === product.id);
        if (cartProductData) {
          cartProducts.push({productData: product, qty: cartProductData.qty});
        }
      }
      res.render('shop/cart', {
        pageTitle: 'Cart',
        path: '/cart',
        products: cartProducts,
      });
    });
  });*/

};

exports.getCheckout = (req, res, next) => {
  res.render('shop/checkout', {
    pageTitle: 'Checkout',
    path: '/checkout',
  });
};

exports.getOrders = (req, res, next) => {
  res.render('shop/orders', {
    pageTitle: 'Orders',
    path: '/orders',
  });
};

exports.getProduct = async (req, res, next) => {
  const prodId = req.params.productId;

  $product = [];
  var getProductById = new Promise((res, rej) => {
    ProductModel.findById(prodId, function(err, rows) {
      if (rows[0]) {
        res(rows[0]);
      }
      res([]);
    });
  });
  $product = await getProductById;

  if ($product.title) {
    res.render('shop/product-detail', {
      product: $product,
      pageTitle: $product.title,
      path: '/products',
    });
  } else {
    res.redirect('/');
  }

};

exports.addToCart = async (req, res, next) => {
  const prodId = req.body.productId;

  $product = [];
  var getProductById = new Promise((res, rej) => {
    ProductModel.findById(prodId, function(err, rows) {
      if (rows[0]) {
        res(rows[0]);
      }
      res([]);
    });
  });
  $product = await getProductById;

 // console.log($product);

  if ($product.title) {
    var $cart_id;
    var $new_cart_id;

    if (req.session.views['cart_id'] > 0) {
      $new_cart_id = req.session.views['cart_id'];
    } else {
      var getLastCartId = new Promise((res, rej) => {
        CartModel.getLastCartId(function(err, rows) {
          if (rows[0]) {
            res(rows[0]);
          }
          res([]);
        });
      });
      $last_cart_id = await getLastCartId;
      if($last_cart_id.id) {

        $new_cart_id = $last_cart_id.id + 1;
      } else {
        $new_cart_id = 1;

      }

      //Init Cart Session
      CartModel.initCart($new_cart_id, function(err, rows) {
        if(!err) {
          req.session.views['cart_id'] = $new_cart_id;
        }
      });

      req.session.views['cart_id'] = $new_cart_id;
    }

    var getCartProductById = new Promise((res, rej) => {
      CartModel.findByProductId(prodId, $new_cart_id,function(err, rows) {
        if (rows[0]) {
          res(rows[0]);
        }
        res([]);
      });
    });
    $productInCart = await getCartProductById;


    if($productInCart.id > 0) {
      $mode = 'update';
    } else {
      $mode = 'add';
    }
    // Add Product in Cart
    CartModel.addProduct(prodId, $new_cart_id, $mode, $product);
    res.redirect('/');
  }
};

exports.deleteCartItem = (req, res, next) => {
  const prodId = req.body.productId;
  //Delete Product
  ProductModel.findById(prodId, product => {
    CartModel.deleteProduct(prodId, product.price);
    res.redirect('/cart');
  });
};



