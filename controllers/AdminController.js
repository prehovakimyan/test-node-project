var jwt = require('jsonwebtoken');
const ProductModel = require('../models/Product');


exports.getAddProduct = (req, res, next) => {
  /*var token = jwt.sign({ foo: 'bar' }, 'shhhhh');
  console.log(token);
  console.log(jwt.verify(token, 'shhhhh'));*/
  res.render('admin/edit-product', {
    pageTitle: 'Add Product',
    path: '/admin/add-product',
    editing: false
  });
};

exports.getEditProduct = async (req, res, next) => {
  const productId = req.params.productId;
  const editMode = req.query.edit;
  if(!editMode) {
    return res.redirect('/');
  }

  $product = [];
  var getProductById = new Promise((res, rej) => {
    ProductModel.findById(productId,function(err, rows) {
      if(rows[0]) {
        res(rows[0]);
      }
      res([]);
    });
  });
  $product = await getProductById;

  if(!$product) {
    return res.redirect('/');
  }

  res.render('admin/edit-product', {
    product: $product,
    pageTitle: 'Edit Product',
    path: '/admin/edit-product',
    editing: editMode
  });

};

exports.updateProduct = (req, res, next) => {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedImageUrl = req.body.imageUrl;
  const updatedPrice = req.body.price;
  const updatedDescription = req.body.description;
  const updatedProduct = new ProductModel(
      prodId,
      updatedTitle,
      updatedImageUrl,
      updatedDescription,
      updatedPrice
  );

  updatedProduct.update();
  res.redirect('/admin/products')
};

exports.getProducts = async (req, res, next) => {
  $products = [];
  var getAllProducts = new Promise((res, rej) => {
    ProductModel.fetchAll(function(err, rows) {
      res(rows)
    });
  });
  $products = await getAllProducts;

  res.render('admin/products', {
    prods: $products,
    pageTitle: 'Admin Products',
    path: '/admin/products',
  });

};

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const imageUrl = req.body.imageUrl;
  const price = req.body.price;
  const description = req.body.description;
  const productItem = new ProductModel(null, title, imageUrl, description, price);
  productItem.save();
  res.redirect('/admin/products');
};
exports.deleteProduct = (req, res, next) => {
  const prodId = req.body.productId;
  ProductModel.delete(prodId);
  res.redirect('/admin/products');
};