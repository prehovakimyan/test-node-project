const path = require('path');
const express = require('express');
const rootDir = require('../util/path');
const adminData = require('./admin');
const ShopController = require('../controllers/ShopController');
const router = express.Router();

router.get('/', ShopController.getIndex);

router.get('/products', ShopController.getProducts);

router.get('/products/:productId', ShopController.getProduct);

router.get('/cart', ShopController.getCart);

router.post('/cart', ShopController.addToCart);

router.post('/cart-delete-item', ShopController.deleteCartItem);

router.get('/checkout', ShopController.getCheckout);

router.get('/orders', ShopController.getOrders);

module.exports = router;