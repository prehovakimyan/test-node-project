const path = require('path');
const express = require('express');
const rootDir = require('../util/path');
const AdminController = require('../controllers/AdminController');

const router = express.Router();

const products = [];

router.get('/add-product', AdminController.getAddProduct);

router.get('/edit-product/:productId', AdminController.getEditProduct);

router.post('/edit-product', AdminController.updateProduct);

router.get('/products', AdminController.getProducts);

router.post('/add-product', AdminController.postAddProduct);

router.post('/delete-product', AdminController.deleteProduct);

module.exports = router;