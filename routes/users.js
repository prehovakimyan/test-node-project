const path = require('path');
const express = require('express');
const rootDir = require('../util/path');
const adminData = require('./admin');
const router = express.Router();

router.get('/users', (req, res, next) => {
  res.render('shop', {prods: products, pageTitle: 'Shop 55', path: '/', hasProducts: products.length > 0, activeShop: true, productCss: true});
});

router.post('/add-users', (req, res, next) => {
  res.redirect('/users');
});

module.exports = router;